import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './AccountApp/LandingPage/landingPage.component';
import { SignupComponent } from './AccountApp/SignupPage/signup.component';
import { LoginComponent } from './AccountApp/LoginPage/login.component';
import { HeaderComponent } from './UtilityApp/Header/header.component';
import { FooterComponent } from './UtilityApp/Footer/footer.component';
import { UserMasterComponent } from './AdminApp/UserMaster/usermaster.component';
import { ConfirmMobileComponent } from './AccountApp/ConfirmMobile/confirm-mobile.component';


//Dashboard & internal pages
import { DashboardComponent } from './DashboardApp/Dashboard/dashboard.component'



//Service
import { AccountService } from './AccountApp/Services/account.service'
import { LoginService } from './AccountApp/Services/login.service'
import { SignupService } from './AccountApp/Services/signup.service'
import { ConfirmMobleService } from './AccountApp/Services/confirm-mobile.service'


import { LocalstorageService } from './UtilityApp/Services/localstorage.service';
import { SharedService } from './UtilityApp/Services/shared.service';
import { NotificationService } from './UtilityApp/Services/notification.service';
import { Broadcaster } from './UtilityApp/Services/broadcaster';
import { MessageEvent } from './UtilityApp/Services/message_event';

import { Http, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from './UtilityApp/Services/http.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,

    FooterComponent,
    DashboardComponent,
    UserMasterComponent,
    LandingPageComponent,
    SignupComponent,
    LoginComponent,
    ConfirmMobileComponent,

    DashboardComponent


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    AccountService,
    LocalstorageService,
    LoginService,
    SignupService,
    ConfirmMobleService,
    SharedService,
    Broadcaster,
    MessageEvent,
    NotificationService, {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function httpFactory(backend: XHRBackend, options: RequestOptions) {
  return new HttpService(backend, options);
}
