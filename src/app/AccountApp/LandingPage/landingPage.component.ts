import { Component } from '@angular/core';
import { Login, Registration, CheckMobileNumber } from '../Models/account';
import { AccountService } from '../Services/account.service';
import { Router } from "@angular/router";
import { NotificationService } from '../../UtilityApp/Services/notification.service';
import { SharedService } from '../../UtilityApp/Services/shared.service';
import { LocalstorageService } from '../../UtilityApp/Services/localstorage.service';
import {Country} from '../../UtilityApp/Services/Country';

@Component({
  selector: 'landing-page',
  templateUrl: './landingPage.component.html'
})

export class LandingPageComponent {

  checkMobileNumber: CheckMobileNumber;
  _userDetails: any;
  _token: string;
  countries: any[]
  countryData: any;
  MobileNo: any;

  constructor(private accountService: AccountService, private router: Router, private notificationService: NotificationService,
    private sharedService: SharedService, private localstorageService: LocalstorageService) {
    this._userDetails = localStorage.getItem('userData');
    if (this._userDetails != undefined) {
      this._token = JSON.parse(this._userDetails).token;
    }

    //this.countries = [{ Id: "0", Name: "India", CountyCode: "+91", Flag:"src/assets/img/flags/16/India.png" }, { Id: "1", Name: "Singapore", CountyCode: "+65", Flag:"src/assets/img/flags/16/India.png" }];
   
     
    this.countries = this.sharedService.getCountries();

    this.countryData = this.countries[0].CountyCode;

     this.MobileNo = "7798554631"; 

  //   this.countryData = '+91';

    localstorageService.setData("country", this.countries[0]);
  }

  getMobileNumberStatus(mobileNo: number, countryCode: number) {

    this.sharedService.setData("mobileNumber", mobileNo);
    this.sharedService.setData("countryCode", countryCode);
    this.notificationService.success("WelCome", "Institute management platform");
    this.router.navigate(['signup']);

    // this.checkMobileNumber = new CheckMobileNumber();
    // this.checkMobileNumber.MobileNumber = mobileNo;

    //   this.accountService.checkMobileNumberIsEixst(this.checkMobileNumber, this._token)
    //     .then(result => {
    //       if (result.status == 202) {

    //       }
    //     });
  }

  changeCountry(id: number) {
    
    this.localstorageService.setData("country", this.countries[id]);
  }
}